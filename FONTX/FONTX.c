/****************************************************************
 * FONTX driver cooperating with 128x64 display ST7920          *
 * Author: Petr Skryja      skryja.petr@seznam.cz               *
 * Date:   08.01.2018                                           *
 ***************************************************************/

#include "FONTX.h"
#include "../ST7920/ST7920.h"


/* Draw pixel to a specific address */
void FONTX_LCD_print_pixel(uint8_t AddrX, uint8_t AddrY, bool SetPixel)
{
    if(SetPixel)
        ST7920l_draw_pixel(AddrX , AddrY, set_pixel);
    else
        ST7920l_draw_pixel(AddrX , AddrY, reset_pixel);
}

/* Draw one byte (8pixels) to a specific address */
void FONTX_LCD_print_8pixels(uint8_t AddrX, uint8_t AddrY, uint8_t Pixels)
{
    ST7920_draw_8pixels(AddrX, AddrY, Pixels, rewrite_pixel);
}

/* Write string (format 8x13) to a specific address */
/* If the character does not fit on the line will not be drawn */
void FONTX_puts_8x13(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, const char *S)
{
    char C;
    uint8_t Width = FontxChar->FontWidth;
    uint32_t XOffset = 0u;

    while( (C = *S++))
    {
        if((AddrX + Width * (XOffset + 1u)) < FONTX_DISPLAY_WIDTH) /* check position of char */
        {
            FONTX_print_char_8x13(AddrX + XOffset * Width, AddrY, FontxChar, C);
        }
        else
        {
            /* char is out of display, do nothing */
        }

        XOffset++;
    }
}

/* Write string to a specific address */
/* If the character does not fit on the line will not be drawn */
void FONTX_puts(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, const char *S)
{
    char C;
    uint8_t Width = FontxChar->FontWidth;
    uint32_t XOffset = 0u;

    while( (C = *S++))
    {
        if((AddrX + Width * (XOffset + 1u)) < FONTX_DISPLAY_WIDTH) /* check position of char */
        {
            FONTX_print_char(AddrX + XOffset * Width, AddrY, FontxChar, C);
        }
        else
        {
            /* char is out of display, do nothing */
        }

        XOffset++;
    }
}

/* Draw one FONTX 8x13 char to a specific address (from the bottom left corner to right upper corner)*/
/* NumberOfChar - char number in FONTX table */
void FONTX_print_char_8x13(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, uint8_t NumberOfChar)
{

    uint8_t YShifter;
    for(YShifter = 0u; YShifter < 13u; YShifter++)
    {
        FONTX_LCD_print_8pixels(AddrX, AddrY - 12u + YShifter, FontxChar->FontImage[13u * NumberOfChar + YShifter]);
    }
}

/* Draw one FONTX char to a specific address (from the bottom left corner to right upper corner) */
void FONTX_print_char(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, uint8_t NumberOfChar)
{
    uint8_t Height = FontxChar->FontHeight;
    uint8_t Width = FontxChar->FontWidth;

    /*Calculation number of bytes in one row*/
    uint8_t BytesInRow;
    if(Width % 8u)
        BytesInRow = Width / 8u + 1u;
    else
        BytesInRow = Width / 8u;

    /*Char offset in FontImage array*/
    uint32_t CharOffset = BytesInRow * Height * NumberOfChar;

    uint8_t XShifter, YShifter;
    for(YShifter = 0u; YShifter < Height; YShifter++)
    {
        for(XShifter = 0u; XShifter < Width; XShifter++)
        {
            uint32_t BytePosition = CharOffset + XShifter/8u + YShifter*BytesInRow; /*Position of byte in FontImage array*/
            bool PrintPixel = FONTX_get_pixel_from_byte(FontxChar->FontImage[BytePosition], XShifter % 8u); /*Set/Reset pixel?*/
            FONTX_LCD_print_pixel(AddrX + XShifter, AddrY - Height + 1u + YShifter, PrintPixel); /*Show pixel on display*/
        }
    }
}

/* Function return true, if the bit on Position is log "1" */
bool FONTX_get_pixel_from_byte(uint8_t ByteRegister, uint8_t Position)
{
    if((ByteRegister >> (7u - Position)) & 0x01)
        return true;
    else
        return false;
}

