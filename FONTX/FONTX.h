/****************************************************************
 * FONTX driver cooperating with 128x64 display ST7920          *
 * Author: Petr Skryja      skryja.petr@seznam.cz               *
 * Date:   08.01.2018                                           *
 ***************************************************************/

#ifndef FONTX_H_INCLUDED
#define FONTX_H_INCLUDED

#include "stm32f4xx_hal.h"
#include <stdbool.h>

#define	IMPORT_BIN(file, type, sym)\
        __asm__ (\
        ".section \".rodata\"\n"\
        ".balign 4\n"\
        ".global " #sym "\n"\
        #sym ":\n"\
        ".incbin \"" file "\"\n"\
        ".global _sizeof_" #sym "\n"\
        ".set _sizeof_" #sym ", . - " #sym "\n"\
        ".balign 4\n"\
        ".section \".text\"\n");\
        extern type sym[]


#define FONTX_DISPLAY_WIDTH 128

typedef struct
{
    char FileSignature[6];  /* "FONTX2" */
    char FontName[8];
    uint8_t FontWidth;
    uint8_t FontHeight;
    uint8_t CodeFlag;
    uint8_t FontImage[];
} fontx_st;

void FONTX_print_char(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, uint8_t NumberOfChar);
void FONTX_print_char_8x13(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, uint8_t NumberOfChar);
void FONTX_puts_8x13(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, const char *S);
void FONTX_puts(uint8_t AddrX, uint8_t AddrY, fontx_st *FontxChar, const char *S);
void FONTX_LCD_print_8pixels(uint8_t AddrX, uint8_t AddrY, uint8_t Pixels);
void FONTX_LCD_print_pixel(uint8_t AddrX, uint8_t AddrY, bool SetPixel);
bool FONTX_get_pixel_from_byte(uint8_t ByteRegister, uint8_t Position);
void FONTX_get_pixels_into_byte(uint8_t AddrX, uint8_t AddrY, uint8_t ByteRegister, uint8_t Position, uint8_t NumberOfPositions);



#endif /* FONTX_H_INCLUDED */
