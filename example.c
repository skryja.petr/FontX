/* ... */
 
#include "ST7920.h"
#include "FONTX.h"
 
IMPORT_BIN("ISO-8859-2_8x13.fnt", fontx_st, Fontx8x13B);
 
/* ... */
 
int main(void)
{
 
    /* peripheral initialization ... */
 
    ST7920_init();
    ST7920_clear_graphic();
 
    FONTX_print_char(0u, 12u, &Fontx8x13B, 'a');
    FONTX_print_char(3u, 25u, &Fontx8x13B, 0xEC);
    FONTX_print_char(6u, 38u, &Fontx8x13B, 236u);
 
    FONTX_print_char_8x13(8u, 12u, &Fontx8x13B, 'a');
    FONTX_print_char_8x13(11u, 25u, &Fontx8x13B, 0xEC);
    FONTX_print_char_8x13(14u, 38u, &Fontx8x13B, 236u);

    FONTX_puts(0u, 12u, &Fontx8x13B, "abcd");
 
    FONTX_puts_8x13(0u, 38u, &Fontx8x13B, "abcd");
 
    while(1)
    {
	/* ... */
    }
}